# Metatranscript coverage project


## Description

The goal of this script is to create figures showing the average coverage of transcripts in different conditions from Ribo-seq data.


# Prerequisites

For now, to run the pipeline, you must have docker installed and running on your machine. Then, you must build the metatranscript image by executing the command :

```sh
bash src/create_metatranscript_container.sh
```

If you attempt to use singularity (**it must be installed on your computer with docker**), then you'll have to build the singularity images used in this pipeline. To do this, you can run the following command: 

```sh
bash src/build_singularity_img.sh
```

## Getting started 

To install nexflow on your machine, you must have an internet access, Java 8 (or later, up to 11) and Bash 3.2 (or later) installed.
Then you can run this command:

```sh
bash src/install_nextflow.sh
```

To download the tiny dataset used to test the pipeline run :

```sh
bash src/download_tiny_dataset.sh
```

You can then test the pipeline by launching the following command:

```sh
bash src/nf/test.sh
```

This will run the pipeline using the docker and the singularity profile.
If you don't get errors, everything is OK.


To launch the test pipeline on the psmn run :

```sh
bash src/nf/test_psmn.sh
```

## Usage

To use the pipeline, your command must have the following form:

```sh
./bin/nextflow src/nf/metagene_pipeline.nf -c src/nf/metagene_pipeline.config -profile PROFILE PARAMS
```

This command must be launched from the root of the metatranscript_coverage directory.

`PROFILE`: Can take those value : 

* docker : can be used in your local machine
* singularity : can be used in your local machine
* psmn : to use the pipeline on the psmn

`PARAMS` : Parameters of the pipeline.


| Required parameters  |                   description                         | type | default |
|:--------------------:|:-----------------------------------------------------:|:-:|:-:|
|        --fasta       | A fasta file corresponding to a transcriptome         | File | $PWD/data/transcriptome/*.fa |
|        --fastq       | Fastq files that will be used in the analysis. This parameter allows globbing | Files | $PWD/data/fastq/*.fastq |
|     --annotation     | A File containing annotation for each transcripts in the transcriptome file. A bed file is supported | File | $PWD/data/annotation/appris_mouse_v1_actual_regions.bed |



The `annotation` file must have at least 4 columns:

1. The first column must contain the name of the transcript
2. The second must contain the start position of a region in the transcript : \
(i.e the start position of an utr for example)
3. The third column must contain the end position of the region
4. The last column must contain the name of the region (UTR5, CDS or UTR3)

The columns must be separated by a tablulation.

| Optional parameters  |                   description                         | type | default |
|:--------------------:|:-----------------------------------------------------:| :-:|:-:|
| --normalization      | The normalization method to apply on the transcript. See deeptools documentation to chose the normalization method (it can take the following values RPKM, CPM, BPM, RPGC, None) | string | CPM |
| --pool_file      | A file used to pool samples together the number of categories of transcripts you want to create. You can enter `None` if you don't want to pull samples together| File, string | None |
| --transcript_group_number      | The number of transcript groups you want to have. One coverage figure will be created for each transcript group. A group of transcript corresponds to transcripts with similar size. | int | 5 |
| --bin_numbers      | The number of bin we want to use for each category of transcript. You can entrer '-1' to automatically compute a number of bins a category of transcript. If you have 3 group of transcript and you whant to set 20 bins for the smallest transcripts, 30 bins for the medium transcripts and 40 bins for the largest you can entrer | string | "-1" |
| --trim      | The proportion of the smallest and largest transcripts to remove. You can enter 0.025 to remove 2.5% of the smallest and largest transcript or 0 to not remove anything | float | 0.025
| --project_name      | Name of the project, used in result file names | string | metatranscript_analysis |
| --min_avg_coverage      | Minimum coverage a transcript must have to be considered | float | 0.5 |
| --nt_wrap      | The number of nucleotides right before or right after the CDS of transcript to display the the coverage figure. Important note : The transcripts having their 5'UTR or 3'UTR smaller than this number wont be considered | int | 50 |
| --ci      | The size of cofidence interval to display for each bin. You can set it to 0 to hide the confidence interval. The confidence interval is built by resampling the coverage values of transcripts at each bin with replacement. The mean value is then calculated. The process is repeated 1000 times and it's one those value that the confidence interval is computed | int | 0 |


The  `pool_file` must contain two columns separated by a tabulation:

1. The first column must contain the basename of the sample (i.e the \
file name but without the extention)
2. The second column must contain the name of the group to which the \
sample belongs