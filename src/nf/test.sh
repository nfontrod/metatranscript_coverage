./bin/nextflow src/nf/metagene_pipeline.nf \
  -c src/nf/metagene_pipeline.config \
  -profile singularity \
  --fasta "data/tiny_dataset/fasta/tiny_v2.fasta" \
  --fastq "data/tiny_dataset/fastq/tiny*_S.fastq" \
  --annotation "data/tiny_dataset/annot_tr.bed" \
  -resume


./bin/nextflow src/nf/metagene_pipeline.nf \
  -c src/nf/metagene_pipeline.config \
  -profile docker \
  --fasta "data/tiny_dataset/fasta/tiny_v2.fasta" \
  --fastq "data/tiny_dataset/fastq/tiny*_S.fastq" \
  --annotation "data/tiny_dataset/annot_tr.bed" \
  -resume
