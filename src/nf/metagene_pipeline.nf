/** A fasta file corresponding to a transcriptome

@type File 
*/
params.fasta = "$PWD/data/transcriptome/*.fa"

/** Fastq files that will be used in the analysis. This parameter allows \ 
globbing

@type: Files
*/
params.fastq = "$PWD/data/fastq/*.fastq"

/** A File containing annotation for each transcripts in the transcriptome \
file. A bed file is supported. The file must have at least 4 columns:

1. The first column must contain the name of the transcript
2. The second must contain the start position of a region in the transcript : \
(i.e the start position of an utr for example)
3. The third column must contain the end position of the region
4. The last column must contain the name of the region (UTR5, CDS or UTR3)

The columns must be separated by a tablulation.

@type File
*/
params.annotation="$PWD/data/annotation/appris_mouse_v1_actual_regions.bed"

/** The normalization method to apply on the transcript. See deeptools \
documentation to chose the normalization method.

@type string
@range [RPKM, CPM, BPM, RPGC, None]
*/
params.normalization = "CPM"

/** A file used to pool samples together the number of categories \
of transcripts you want to create. You can enter None if you don't want \
to pull samples together. If you give a file, it must contain two columns \ 
separated by a tabulation:

1. The first column must contain the basename of the sample (i.e the \
file name but without the extention)
2. The second column must contain the name of the group to which the \
sample belongs

@type [File, string]
*/
params.pool_file = "None"  

/** The number of transcript groups you want to have. One coverage figure \
will be created for each transcript group. 

@type int
*/

params.transcript_group_number = 5

/** The number of bin we want to use for each category  \
of transcript. You can entrer '-1' to automatically compute a number of \
bins a category of transcript. If you have 3 group of transcript and you \
whant to set 20 bins for the smallest transcripts, 30 bins for the medium \
transcripts and 40 bins for the largest you can entrer \
--bin_numbers "20 30 40".

@type string
*/
params.bin_numbers = "-1"

/* The proportion of the smallest and largest transcripts 
to remove. You can enter 0.025 to remove 2.5% of the smallest and largest \
transcript or 0 to not remove anything. 

@type float
*/
params.trim = 0.025

/* Name of the project, used in result file names 

@type: string
*/
params.project_name = "metatranscript_analysis" 

/* Minimum coverage a transcript must have to \
    be considered (default 0.5)

@type float
*/
params.min_avg_coverage = 0.5

/** The number of nucleotides right before or right after the CDS of \
transcript to display the the coverage figure. Important note : \
The transcripts having their 5'UTR or 3'UTR smaller than this number \
wont be considered.

@type: int
*/
params.nt_wrap = 50

/** The size of cofidence interval to display for each bin. You can set it \
to 0 to hide the confidence interval. The confidence interval is built \
by resampling the coverage values of transcripts at each bin with replacement.
The mean value is then calculated. The process is repeated 1000 times and \
it's one those value that the confidence interval is computed

@type int
@range [0, 1, ..., 99, 100]
*/
params.ci = 0


/* log data */

log.info "fasta files : ${params.fasta}"
log.info "fastq files : ${params.fastq}"
log.info "annotation file : ${params.annotation}" 
log.info "Normalization method in bigwig files : ${params.normalization}"
log.info "File used to pool sample : ${params.pool_file}"
log.info "Number of category of transcripts : ${params.transcript_group_number}"
log.info """Number of bins to use for each category of transcripts :
 ${params.bin_numbers}""".replace('\n', '')
log.info """Proportion of the smallest and largest transcript to remove : 
  " ${params.trim}""".replace('\n', '')
log.info "Project name: ${params.project_name}"
log.info "Minimum coverage of transcripts : ${params.min_avg_coverage}"
log.info """Number of nucleotides surrounding transcript CDS for which 
to display the coverage: ${params.nt_wrap}""".replace('\n', '')
log.info "Confidence  interval : ${params.ci}"


/* 
 * fasta indexing
 */

Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any fasta files matching: ${params.fasta}" }
  .set { fasta_file }

process index_fasta {
  tag "$fasta.baseName"
  publishDir "results/mapping/index/", mode: 'copy'

  input:
    file fasta from fasta_file

  output:
    file "*.index*" into index_files
    file "*_report.txt" into indexing_report

  script:
"""
bowtie-build --threads ${task.cpus} -f ${fasta} ${fasta.baseName}.index &> ${fasta.baseName}_bowtie_report.txt

if grep -q "Error" ${fasta.baseName}_bowtie_report.txt; then
  exit 1
fi
"""
}


/*
 * mapping single end fastq
 */

Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { fastq_files }

process mapping_fastq {
  tag "$file_id"
  publishDir "results/mapping/bams/", mode: 'copy'

  input:
  set file_id, file(reads) from fastq_files
  file index from index_files.collect()

  output:
  set file_id, "*.bam" into bam_files
  file "*_report.txt" into mapping_report

  script:
index_id = index[0]
for (index_file in index) {
  if (index_file =~ /.*\.1\.ebwt/ && !(index_file =~ /.*\.rev\.1\.ebwt/)) {
      index_id = ( index_file =~ /(.*)\.1\.ebwt/)[0][1]
  }
}
"""
bowtie -m 1 -v 2 --seedlen 23 --best --phred33-quals \
--sam -p ${task.cpus} ${index_id} \
-q ${reads} 2> \
${file_id}_bowtie_report_tmp.txt | \
samtools view -Sb -F 4 > ${file_id}.bam

if grep -q "Error" ${file_id}_bowtie_report_tmp.txt; then
  exit 1
fi
tail -n 19 ${file_id}_bowtie_report_tmp.txt > ${file_id}_bowtie_report.txt
"""
}


/*  
 * bam indexing 
 */

process indexing_bam {
  tag "$file_id"
  publishDir "results/mapping/sorted_bams/", mode: 'copy'
  
  input:
   set file_id, file(bam) from bam_files
  
  output:
    set file_id, "*.sort.bam" into sorted_bam_files
    set file_id, "*.sort.bam.bai" into index_bam_files


  script:
    """
    samtools sort ${bam} > ${bam.baseName}.sort.bam \
    && samtools index ${bam.baseName}.sort.bam
    """
}


/* 
 * creating bigwig
 */

indexed_bam_files = sorted_bam_files.join(index_bam_files, by: 0)

process bam2bigwig {
  publishDir "results/mapping/bigwig/", mode: 'copy'

  input:
   set file_id, file(bam), file(idx) from indexed_bam_files
  
  output:
   file "*.bw" into bigwig_files
   file "*_report.txt" into bigwig_reports
  
  script:
    """
    bamCoverage -b ${bam} -o ${file_id}.bw -bs 1 -p ${task.cpus} \
    --normalizeUsing ${params.normalization} &> ${file_id}_report.txt

    if grep -q "Error" ${file_id}_report.txt; then
      exit 1
    fi
    """
}


/* 
 * creating coverage figure for transcripts
 */

Channel
  .fromPath( params.annotation )
  .ifEmpty { error "Cannot find any annotation files matching: ${params.annotation}" }
  .set { annot_files }


if (params.pool_file == "None"){
  Channel
    .from( params.pool_file )
    .set { pool_files }
} else {
  Channel
    .fromPath( params.pool_file )
    .ifEmpty { error "Cannot find any pool files matching: ${params.pool_file}" }
    .set { pool_files }
  }


process metatranscript {
  publishDir "results/metatranscript", mode: 'copy'

  input:
    file(bw) from bigwig_files.collect()
    file(pool_file) from pool_files.collect()
    file(annot) from annot_files.collect()
  
  output:
    file "*.pdf" into figure_files
    file "*.txt" into report_files
  
  script:
  if(params.pool_file == "None") {
    """
    python3 -m metatranscript -b ${bw} -T ${params.transcript_group_number} \
    -B ${params.bin_numbers} -tr ${params.trim} \
    -P ${params.project_name} -m ${params.min_avg_coverage} \
    -n ${params.nt_wrap} \
    -c ${params.ci} -t ${annot} \
    &> ${params.project_name}_stdin.txt

    if grep -q "Error" ${params.project_name}_stdin.txt; then
      exit 1
    fi
    """
  } else {
    """
    python3 -m metatranscript -b ${bw} -T ${params.transcript_group_number} \
    -B ${params.bin_numbers} -p ${pool_file} -tr ${params.trim} \
    -P ${params.project_name} -m ${params.min_avg_coverage} \
    -n ${params.nt_wrap} \
    -c ${params.ci} -t ${annot} \
    &> ${params.project_name}_stdin.txt

    if grep -q "Error" ${params.project_name}_stdin.txt; then
      exit 1
    fi
    """
  }
}
