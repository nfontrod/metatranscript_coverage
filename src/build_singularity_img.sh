#!/bin/bash

# Building metatranscript docker image
bash src/create_metatranscript_container.sh
cd ./bin

# Converting docker image to a tar file and then to a singularity image
########################################################################
# For singularity v3.0+
# docker image save lbmc/metatranscript:0.1 -o m.tar
# singularity build lbmc-metatranscript-0.1.img docker-archive://m.tar
#rm m.tar
########################################################################


# Creating a local docker registry, and pushing metatranscript image on in 
# and then building the singularity image with this registry
# For singularity 2.6.1
docker container run -d -p 5000:5000 --rm --name registry registry:2
docker tag lbmc/metatranscript:0.1 localhost:5000/lbmc/metatranscript:0.1
docker push localhost:5000/lbmc/metatranscript
cat << EOF > SigFile
Bootstrap: docker
From: lbmc/metatranscript:0.1
Registry: localhost:5000
Namespace:

EOF

sudo SINGULARITY_NOHTTPS=true singularity build lbmc-metatranscript-0.1.img SigFile
rm SigFile
docker container stop $(docker container ls | grep "registry" | cut -f1 -d" ")

# Building the other singularity image needed
singularity build lbmc-bowtie-1.2.2.img docker://index.docker.io/lbmc/bowtie:1.2.2
singularity build lbmc-deeptools-3.0.2.img docker://index.docker.io/lbmc/deeptools:3.0.2
cd ..
