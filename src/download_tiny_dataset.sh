#!/bin/bash
cd data
git clone git@gitbio.ens-lyon.fr:LBMC/hub/tiny_dataset.git
cd tiny_dataset
cat << EOF > annot_tr.bed
NC_000001a	0	20	UTR5	0	+
NC_000001a	20	2990	CDS	0	+
NC_000001a	2990	3001	UTR3	0	+
NC_000001	0	484	UTR5	0	+
NC_000001	484	127001	CDS	0	+
NC_000001	127001	128001	UTR3	0	+
EOF
cd ../..