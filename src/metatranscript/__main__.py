#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: create the metatranscripts coverage figure
"""


import lazyparser as lp
from typing import List
from .metatranscript_functions import metatranscripts_figures_creation
from pathlib import Path
import warnings


@lp.parse(bigwig_files="file", transcript_group_number=range(1, 31),
          bin_numbers="bin_numbers >= -1", trim="0 <= trim <= 1",
          ci="0 <= ci <= 100", min_avg_coverage="min_avg_coverage >= 0")
def launcher(bigwig_files: List[str],
             transcript_annotation: str,
             transcript_group_number: int = 5,
             bin_numbers: List[int] = [-1],
             pool_file: str = "None",
             trim: float = 0.025,
             project_name: str = "metatranscript_analysis",
             min_avg_coverage: float = 0.5,
             ci: int = 90, nt_wrap: int = 0):
    """
    Create metatranscript coverage figure.

    :param bigwig_files: A list of bigwig file to analyse
    :param transcript_annotation: A file that contains the \
    transcript annotations.
    :param transcript_group_number: The number of categories of transcript \
    we want to create (default is 5)
    :param bin_numbers: The number of bin we want to use for each category \
    of transcript. You can entrer -1 to automatically compute a number of \
    bins a category of transcript. (default is -1 for each category of \
    transcript).
    :param pool_file: A file used to pool samples together. If it's None,
    it won't be taken into account.
    :param trim: The proportion of the smallest and largest transcripts \
    to remove. (default is 0.025 so 95 percent of transcript are keeped).
    :param project_name: The name of the project (default is \
    'metatranscript_analysis')
    :param min_avg_coverage: Minimum coverage a transcript must have to \
    be considered (default 0.5)
    :param ci: Confidence interval in percent. You can enter 0 \
    to hide confidence interval from your figure (default 90)
    :param nt_wrap: The number of nucleotide that will not be \
    binned at the beginning and the end of each transcript
    """
    print(f"""parameters used:
                  --bigwig_files {', '.join(bigwig_files)}
                  --transcript_annotation {transcript_annotation}
                  --transcript_group_number {transcript_group_number}
                  --bin_numbers {bin_numbers}
                  --pool_file {pool_file}
                  --trim {trim}
                  --project_name {project_name}
                  --min_avg_coverage {min_avg_coverage}
                  --ci {ci}
                  --nt_wrap {nt_wrap}""")
    if pool_file == "None":
        pool_file = None
    elif not Path(pool_file).is_file():
        warnings.warn(f"The file {pool_file} wasn't found, it won't be used")
        pool_file = None
    transcript_annotation = Path(transcript_annotation)
    metatranscripts_figures_creation(bigwig_files, transcript_annotation,
                                     transcript_group_number,
                                     bin_numbers, pool_file, trim,
                                     project_name,
                                     min_avg_coverage,
                                     ci, nt_wrap)


launcher()
