lazyparser==0.2.0
pyBigWig==0.3.17
numpy==1.17.4
pandas==1.0.3
matplotlib==3.1.2
seaborn==0.10.1