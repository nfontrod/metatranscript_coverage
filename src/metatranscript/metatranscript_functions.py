#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create metatranscript coverage \
figures from different bigwig size given in input.
"""

from typing import List, Tuple, Dict, Optional
import pyBigWig as pbw
import numpy as np
import doctest
from math import sqrt
import pandas as pd
import warnings
from pathlib import Path
import seaborn as sns
import matplotlib.pyplot as plt


class BadReferenceError(Exception):
    pass


def get_annotation(annotation_file: Path,
                   nt_wrap: int) -> Dict:
    """
    Turn an annotation file into a dictionary.

    :param annotation_file: An annotation file containing the \
    size of 5'UTR, CDS and 3'UTR for each transcript.
    :param nt_wrap: The nucleotide
    :return: A dictionary containing the interval and the size of the cds and \
    the size of 3' and 5'UTR

    >>> annot = Path(__file__).parents[2] / "data" / "tiny_dataset" / "annot_tr.bed"
    >>> d = get_annotation(annot, 0)
    >>> d == {'NC_000001a': {'UTR5': 150, 'CDS': [150, 2851, 2701],
    ... 'UTR3': 150}, 'NC_000001': {'UTR5': 150, 'CDS': [150, 127001, 126851],
    ... 'UTR3': 1000}}
    True
    >>> d2 = get_annotation(annot, 50)
    >>> d2 == d
    True
    """
    dic = {}
    transcript_short_utr = []
    with annotation_file.open("r") as annot:
        for line in annot:
            line = line.replace("\n", "").split("\t")
            if line[0] not in dic.keys():
                dic[line[0]] = {}
            if line[3] == "UTR5":
                dic[line[0]][line[3]] = int(line[2])
            elif line[3] == "UTR3":
                dic[line[0]][line[3]] = int(line[2]) - int(line[1])
            else:
                dic[line[0]][line[3]] = [int(line[1]), int(line[2]),
                                         int(line[2]) - int(line[1])]
    return dic


def check_annotation(bigwig_file: str, annotation_file: Path, nt_wrap: int,
                     ) -> Dict:
    """
    Check if the transcript in the annotation are the same as the \
    one in the bigwig
    :param bigwig_file: A bigwig file
    :param annotation_file: An annotation file containing the \
    size of 5'UTR, CDS and 3'UTR for each transcript.
    :param nt_wrap: The nucleotide
    :return: A dictionary containing the interval of the cds and \
    the size of 3' and 5'UTR.
    """
    d= get_annotation(annotation_file, nt_wrap)
    bw = pbw.open(bigwig_file)
    if sorted(bw.chroms().keys()) != sorted(d.keys()):
        BadReferenceError(f"The file {annotation_file} does not have "
                          f"the same transcripts name than in {bigwig_file}")
    return d


def get_size_range(tr_dic: Dict, trim: float) -> Optional[Tuple[int, int]]:
    """
    Get the interval of size that excluse ``trim`` * 2 * 100 percent \
    of the transcript present in the ``bigwig`` file.

    :param tr_dic: A dictionary containing the interval of the cds and \
    the size of 3' and 5'UTR
    :param trim: The proportion of smallest and largest transcript to \
    remove. A value of 0.025 will remove 2.5% of the smallest and 2.5% of \
    the longest transcript.
    :return: The interval of size that include ``trim`` * 2 * 100 percent \
    of the initial transcripts
    """
    sizes = [tr_dic[tr]['CDS'][2] for tr in tr_dic]
    if trim == 0:
        return None
    return int(round(np.quantile(sizes, trim))), \
           int(round(np.quantile(sizes, 1 - trim)))


def get_covered_transcripts(bigwig: List[str], min_avg_coverage: float
                            ) -> Dict[str, int]:
    """
    Get the list of covered transcript in a bigwig list, And the list of \
    their size.

    :param bigwig: (list of str) list of bigwig files
    :param min_avg_coverage: Minimum average coverage of the transcript \
    for it to be considered
    :return: A dictionary with as key the name of the transcripts of interest \
    and as value, their size
    """
    if min_avg_coverage == 0:
        bw = pbw.open(bigwig[0])
        transcripts = bw.chroms()
        bw.close()
    else:
        transcripts = {}
        for bw_file in bigwig:
            bw = pbw.open(bw_file)
            for trans in bw.chroms().keys():
                nb = bw.stats(trans, 0, bw.chroms(trans), type="mean",
                              exact=True)[0]
                if nb >= min_avg_coverage and trans not in transcripts.keys():
                    transcripts[trans] = bw.chroms()[trans]
            bw.close()
    return transcripts


def filtering_transcript_dict(transcripts: Dict[str, int],
                              good_interval: Optional[Tuple[int, int]]
                              ) -> Dict[str, int]:
    """
    Removing every transcript for which the size is lower than \
    good_interval[0] or greater than good_interval[1].

    :param transcripts:  A dictionary with as key the name of the \
    transcripts of interest and as value, their size
    :param good_interval: interval of transcript size
    :return: The dictionary but only containing transcript within \
    the interval of size ``good_interval``

    >>> transcripts = {"A": 1, "B": 5, "C": 10, "D": 15, "E": 20}
    >>> filtering_transcript_dict(transcripts, None)
    {'A': 1, 'B': 5, 'C': 10, 'D': 15, 'E': 20}
    >>> filtering_transcript_dict(transcripts, (5, 15))
    {'B': 5, 'C': 10, 'D': 15}
    """
    if good_interval is None:
        return transcripts
    new_dic = {}
    for k, v in transcripts.items():
        if good_interval[0] <= v <= good_interval[1]:
            new_dic[k] = v
    return new_dic


def get_interval_range(sizes: List[int], group_nb: int
                       ) -> List[Tuple[int, int]]:
    """
    Get the interval size of the different groups of transcript.

    :param sizes: The list of transcript size
    :param group_nb: The number of size interval to create for the groups.
    :return: The range of of the different group of transcripts.

    >>> sizes = list(np.concatenate((np.arange(10, 20, 1),
    ... np.arange(20, 32, 2), np.arange(0, 10, 2))))
    >>> get_interval_range(sizes, 2)
    [(0, 15), (15, 31)]
    >>> get_interval_range(sizes, 4)
    [(0, 10), (10, 15), (15, 20), (20, 31)]
    >>> get_interval_range(sizes, 1)
    [(0, 31)]
    """
    quantiles = np.arange(0, 1 + 1/group_nb, 1/group_nb)
    size_quantile = [np.quantile(sizes, q) for q in quantiles]
    size_quantile[-1] += 1  # Add one to the last value
    return [(int(round(size_quantile[i])), int(round(size_quantile[i + 1])))
            for i in range(len(quantiles) - 1)]


def get_transcript_groups(size_intervals: List[Tuple[int, int]],
                          transcripts: Dict[str, Dict]
                          ) -> Dict[str, List[str]]:
    """
    Return different transcript groups according to the size of each \
    transcripts.
    :param size_intervals: An interval containing transcript size interval.
    :param transcripts: The dictionary containing the transcript their coding \
    coordinates and their size and the size of their UTR
    :return: List of transcripts according to their size in ``size_intervals``.

    >>> size_intervals = [(0, 10), (10, 15), (15, 20), (20, 31)]
    >>> sizes = list(np.concatenate((np.arange(10, 20, 1),
    ... np.arange(20, 32, 2), np.arange(0, 10, 2))))
    >>> sizes = [[0, 0, s] for s in sizes]
    >>> letters = "ABCDEFGHIJKLMNOPQRSTU"
    >>> transcript = {a: {"CDS": b} for a, b in zip(letters, sizes)}
    >>> r = get_transcript_groups(size_intervals, transcript)
    >>> for k in r:
    ...     print(k, " : ", r[k])
    [0,10[  :  ['Q', 'R', 'S', 'T', 'U']
    [10,15[  :  ['A', 'B', 'C', 'D', 'E']
    [15,20[  :  ['F', 'G', 'H', 'I', 'J']
    [20,31[  :  ['K', 'L', 'M', 'N', 'O', 'P']
    """
    transcript_groups = {inter[0]: [] for inter in size_intervals}
    for k, size in transcripts.items():
        for min_size, max_size in size_intervals:
            if min_size <= size["CDS"][2] < max_size:
                transcript_groups[min_size].append(k)
    for min_size, max_size in size_intervals:
        transcript_groups[f'[{min_size},{max_size}['] = \
            transcript_groups.pop(min_size)
    return transcript_groups


def get_transcript_dic(tr_dic: Dict, transcripts: List[str]) -> Dict:
    """

    :param tr_dic: A dictionary containing the interval of the cds and \
    the size of 3' and 5'UTR
    :param transcripts: The list of transcript to conserve
    :return: The dictionary tr_dic but only for kept transcripts
    """
    return {key: tr_dic[key] for key in tr_dic.keys()
            if key in transcripts}


def get_bin_number(size_intervals: List[Tuple[int, int]],
                   bin_numbers: List[int]) -> Dict[str, int]:
    """

    :param size_intervals: The range of of the different group of transcripts.
    :param bin_numbers: The number of bins we want for each groups of \
    transcript
    :return: The number of bins we want for each group of transcript

    >>> b = [8]
    >>> size_intervals = [(10, 15), (15, 20), (20, 31)]
    >>> get_bin_number(size_intervals, b)
    {'[10,15[': 8, '[15,20[': 6, '[20,31[': 7}
    >>> get_bin_number(size_intervals, [70])
    {'[10,15[': 5, '[15,20[': 6, '[20,31[': 7}
    """
    if len(bin_numbers) < len(size_intervals):
        bin_numbers += [-1] * (len(size_intervals) - len(bin_numbers))
    elif len(bin_numbers) > len(size_intervals):
        raise ValueError(f"Error: The size of bin_numbers ({len(bin_numbers)}) "
                         f"should be lower or equals to size_interval "
                         f"({len(bin_numbers)})")
    dic_bins = {}
    for i, (min_size, max_size) in enumerate(size_intervals):
        k = f'[{min_size},{max_size}['
        if bin_numbers[i] != -1 and bin_numbers[i] <= min_size:
            dic_bins[k] = bin_numbers[i]
        else:
            dic_bins[k] = int(round(sqrt(size_intervals[i][0]) * 1.5))
    return dic_bins


def create_report(transcript_groups: Dict[str, List[str]],
                  dic_bins: Dict[str, int], project_name: str):
    """

    :param transcript_groups: List of transcripts according to their size \
    in ``size_intervals
    :param dic_bins: The number of bins we want for each group of transcript
    :param project_name: The name of the project
    """
    dic = {"group_id": [], "transcript_group": [], "min_transcript_size": [],
           "max_transcript_size": [], "transcript_number": [],
           "bin_number": [], "min_nt_in_bin": [], "max_nt_in_bin": []}
    for i, k in enumerate(transcript_groups.keys()):
        dic["group_id"].append(f"group-{i}")
        dic["transcript_group"].append(k)
        mins, maxs = k.replace(']', '').replace('[', '').split(',')
        dic['min_transcript_size'].append(mins)
        dic['max_transcript_size'].append(int(maxs) - 1)
        dic["transcript_number"].append(len(transcript_groups[k]))
        dic["bin_number"].append(dic_bins[k])
        dic["min_nt_in_bin"].append(int(int(mins) / dic_bins[k]))
        dic["max_nt_in_bin"].append(int(int(maxs) / dic_bins[k]))
    df = pd.DataFrame(dic)
    df.to_csv(f"{project_name}_report.txt", sep="\t", index=False)


def create_transcript_table(transcripts: List[str],
                            bin_number: int,
                            bigwig: List[str],
                            tr_dic: Dict[str, Dict],
                            nt_wrap: int) -> pd.DataFrame:
    """
    Create a metatranscript table.

    :param transcript_groups: List of transcripts for the group of interest
    :param bin_number: The number of bins to use
    :param group_name:the name of a group of transcript
    :param dic_bins: The number of bins we want for each group of transcript
    :param bigwig: The list of bigwig file of interest
    :param tr_dic: The dictionary containing the cds interval for each \
     transcript
    :param nt_wrap: The number of nucleotide that will not be \
    binned at the beginning and the end of each transcript
    :return: The metatranscript table that will be used to create the figure
    """
    dic = {"bigwig": [], "bin": [], "coverage": [], 'transcript': [],
           'loc': []}
    us = 0.25 # The size of the UTR in the figure
    fs = 1 # the size of the cds part of the figure
    sp = round(1 / bin_number, 6) # space between bins
    for bw_file in bigwig:
        bw = pbw.open(bw_file)
        if "/" in bw_file:
            bw_file = bw_file.split('/')[-1]
        file_name = bw_file.replace('.bw', '')
        for transcript in transcripts:
            vals = bw.stats(transcript, tr_dic[transcript]['CDS'][0],
                            tr_dic[transcript]['CDS'][1], type="mean",
                            nBins=bin_number)
            if nt_wrap > 0:
                start = max(tr_dic[transcript]['CDS'][0] - nt_wrap, 0)
                stop = tr_dic[transcript]['CDS'][0]
                utr5 = bw.values(transcript, start, stop) \
                    if stop > start else []
                if len(utr5) < nt_wrap:
                    utr5 = [np.nan] * (nt_wrap - len(utr5)) + utr5
                start = tr_dic[transcript]['CDS'][1]
                stop = tr_dic[transcript]['CDS'][1] + \
                    min(nt_wrap, tr_dic[transcript]['UTR3'])
                utr3 = bw.values(transcript, start, stop) \
                    if start < stop else []
                if len(utr3) < nt_wrap:
                    utr3 += [np.nan] * (nt_wrap - len(utr3))
                binutr5 = list(np.round(np.arange(-us, 0, us / nt_wrap), 4))
                binutr3 = list(np.round(np.arange(fs + us / nt_wrap,
                                         fs + us / nt_wrap + us,
                                         us / nt_wrap), 4))
            else:
                utr5 = utr3 = binutr5 = binutr3 = []
            full_vals = utr5 + vals + utr3
            full_bin = binutr5 + list(np.round(
                np.arange(0, 1 + sp, (1 + sp) / bin_number), 4)) + binutr3
            dic["bigwig"] += [file_name] * len(full_vals)
            dic['bin'] += full_bin
            dic['coverage'] += full_vals
            dic['loc'] += ["UTR5"] * len(utr5) + ["CDS"] * len(vals) + \
                ['UTR3'] * len(utr3)
            dic['transcript'] += [transcript] * len(full_vals)
    print({k: len(dic[k]) for k in dic})
    df = pd.DataFrame(dic)
    return pd.DataFrame(dic)


def create_lineplot(df: pd.DataFrame, group_transcript_name: str,
                    group_transcript_id: str,
                    project_name: str, ci: int, nt_wrap: int,
                    bin_nb: int):
    """
    Create a lineaplot showing the coverage of each bigwig.

    :param df: Dataframe of coverage
    :param group_transcript_name: The group of transcript currently analyzed
    :param group_transcript_id: The id of the group of transcript currently \
    analyzed
    :param project_name: The name of the prject of interest
    :param ci: Confidence interval in percent. You can enter 0 \
    to hide confidence interval
    :param nt_wrap: The number of nucleotide that will not be \
    binned at the beginning and the end of each transcript
    :param bin_nb: the number of bin of interest
    """
    sns.set()
    sns.set_context("talk")
    plt.figure(figsize=(20, 12))
    if ci == 0:
        ci = None
    ax = sns.lineplot(x="bin", y="coverage", hue="group", markers=True,
                 dashes=False, data=df, ci=ci)
    if nt_wrap != 0:
        loc_min = 0
        loc_max = 1
        min_bin = np.min(df['bin'].values)
        max_bin = np.max(df['bin'].values)
        ax.axvline(x=loc_min, color="k", ls="--")
        ax.axvline(x=loc_max, color="k", ls="--")
        ymin, ymax = ax.get_ylim()
        ax.text(s="5'UTR (nt)", x=np.mean([min_bin, loc_min]), y=ymin,
                horizontalalignment='center', verticalalignment='bottom')
        ax.text(s="CDS (bin)", x=np.mean([loc_max, loc_min]), y=ymin,
                horizontalalignment='center', verticalalignment='bottom')
        ax.text(s="3'UTR (nt)", x=np.mean([loc_max, max_bin]), y=ymin,
                horizontalalignment='center', verticalalignment='bottom')
        plt.xlabel("Bin (CDS) / Nt (UTR's)")
    else:
        plt.xlabel("Bin (CDS)")
    plt.title(f'Coverage of bigwig files for transcripts with the id '
              f'{group_transcript_id} and\nhaving a size in the interval '
              f'{group_transcript_name}')
    plt.savefig(f"{project_name}_{group_transcript_id}_lineplot.pdf")


def get_transcript_number(bigwig: str) -> int:
    """
    Return the number of transcript in a bigwig file

    :param bigwig: A bigwig file
    :return: The number of transcript in a bigwig file.
    """
    bw = pbw.open(bigwig)
    return len(bw.chroms())


def check_transcript_number(bigwig: str, trim: float,
                            transcript_group_number: int) -> Tuple[int, int]:
    """

    :param bigwig: A bigwig file
    :param trim: The proportion of smallest and largest transcript to \
    remove. A value of 0.025 will remove 2.5% of the smallest and 2.5% of \
    the longest transcript.
    :param transcript_group_number: The number of group of transcript size \
    to make
    :return:  The trim and transcript_group_number values
    """
    transcript_number = get_transcript_number(bigwig)
    if trim > 0 and transcript_number < 100:
        warnings.warn("Very few transcripts found, setting --trim to 0 !",
                      UserWarning)
        trim = 0
    if transcript_number / transcript_group_number < 10:
        warnings.warn("Very few transcripts found, setting "
                      "--transcript_group_number to 1 !",
                      UserWarning)
        transcript_group_number = 1
    return trim, transcript_group_number


def check_number_transcript(bigwig_files: List[str]) -> None:
    """
    Check if every bigwig files have the same number of transcript as \
    reference.

    :param bigwig_files: The list of bigwig file of interest
    """
    if len(bigwig_files) > 1:
        res = [get_transcript_number(b) for b in bigwig_files]
        if any(elem != res[0] for elem in res):
            raise BadReferenceError("The number of transcript is not the "
                                    "same between references !")


def check_pool_file(df: pd.DataFrame, pool_file: Path) -> bool:
    """
    Check if pool_frame has a good format

    :param df: A dataframe showing the coverage of each bin for each transcript
    :param pool_file: A file to pool transcript together
    :return: True if pool_file as a good format, false else.
    """
    base_file_name = np.unique(df['bigwig'].values)
    projects = []
    with pool_file.open("r") as f:
        for i, line in enumerate(f):
            line = line.replace('\n', '').split('\t')
            if len(line) != 2:
                warnings.warn(f"Wrong number of column in {pool_file} file, "
                              f"(line {i + 1}). "
                              f"This file won't be taken into account.",
                              UserWarning)
                return False
            elif len(line[1]) == 0:
                warnings.warn(f"The length of the second column should not be "
                              f"0 in line {i + 1} of {pool_file} file. "
                              f"This file won't be taken into account.",
                              UserWarning)
                return False
            projects.append(line[0])
    if sorted(projects) != list(base_file_name):
        warnings.warn(f"Sample names don't match between {pool_file}, "
                      f"and the ones given as bigwig file : "
                      f"{sorted(projects)} != {list(base_file_name)}",
                      UserWarning)
        return False
    return True


def add_group_column(df: pd.DataFrame, pool_file: Path) -> pd.DataFrame:
    """
    The dataframe with a new column group.

    :param df: A dataframe showing the coverage of each bin for each transcript
    :param pool_file: A file to pool transcript together
    :return: The same dataframe with one additional column ``group``
    """
    if not check_pool_file(df, pool_file):
        df['group'] = df['bigwig']
    else:
        grp_df = pd.read_csv(pool_file, sep="\t", names=["bigwig", "group"])
        df = df.merge(grp_df, how="left", on=["bigwig"])
    return df


def create_group_column(df: pd.DataFrame, pool_file: Optional[Path]
                        ) -> pd.DataFrame:
    """
    Add a column 'group' in the dataframe ``df`` to pool the sample together.

    :param df: A dataframe showing the coverage of each bin for each transcript
    :param pool_file: A file to pool transcript together
    :return: The same dataframe with one additional column ``group``
    """
    if pool_file is None:
        df['group'] = df['bigwig']
    elif not pool_file.is_file():
        warnings.warn(f"The file {pool_file} wasn't found ! "
                      f"The bigwig files won't be pooled together")
        df['group'] = df['bigwig']
    else:
        df = add_group_column(df, pool_file)
    return df



def metatranscripts_figures_creation(bigwig_files: List[str],
                                     transcript_annotation: Path,
                                     transcript_group_number: int,
                                     bin_numbers: List[int],
                                     pool_file: Optional[str],
                                     trim: float = 0.025,
                                     project_name: str =
                                     "metatranscript_analysis",
                                     min_avg_coverage: float = 0.5,
                                     ci: int = 90,
                                     nt_wrap: int = 0):
    """

    :param bigwig_files: A list of bigwig files
    :param transcript_annotation: A file that contains the \
    transcript annotations.
    :param transcript_group_number: The number of group of transcript size \
    to make
    :param trim: The proportion of smallest and largest transcript to \
    remove. A value of 0.025 will remove 2.5% of the smallest and 2.5% of \
    the longest transcript.
    :param project_name: The name of the project of interest.:
    :param bin_numbers: The number of bins we want for each transcript \
    groups. If -1 the number of bins is determined automatically.
    :param pool_file: A file used to pool samples together
    :param min_avg_coverage: Minimum average coverage of the transcript \
    for it to be considered
    :param ci: Confidence interval in percent. You can enter 0 \
    to hide confidence interval
    :param nt_wrap: The number of nucleotide that will not be \
    binned at the beginning and the end of each transcript
    """
    pool_file = Path(pool_file) if pool_file is not None else None
    check_number_transcript(bigwig_files)
    dic_transcript = \
        check_annotation(bigwig_files[0], transcript_annotation, nt_wrap)
    print(f"{get_transcript_number(bigwig_files[0])} transcripts found !")
    trim, transcript_group_number = \
        check_transcript_number(bigwig_files[0], trim, transcript_group_number)
    good_interval = get_size_range(dic_transcript, trim)
    transcripts = get_covered_transcripts(bigwig_files, min_avg_coverage)
    transcripts = filtering_transcript_dict(transcripts, good_interval)
    transcripts = get_transcript_dic(dic_transcript, list(transcripts.keys()))
    print(f"{len(transcripts)} transcripts keeped !")
    size_intervals = get_interval_range([transcripts[k]['CDS'][2]
                                         for k in transcripts],
                                        transcript_group_number)
    print(f"Selected CDS size interval {size_intervals}")
    transcript_groups = get_transcript_groups(size_intervals, transcripts)
    print("Number of transcripts per groups %s" % {k: len(transcript_groups[k])
                                                   for k in transcript_groups})
    dic_bins = get_bin_number(size_intervals, bin_numbers)
    print(f"Bin size per groups {dic_bins}")
    create_report(transcript_groups, dic_bins, project_name)
    for i, transcript_name in enumerate(transcript_groups):
        df = create_transcript_table(transcript_groups[transcript_name],
                                     dic_bins[transcript_name], bigwig_files,
                                     transcripts, nt_wrap)
        df = create_group_column(df, pool_file)
        df.to_csv(f"group-{i}_project_name_table.txt", sep="\t", index=False)
        create_lineplot(df, transcript_name, f"group-{i}", project_name,
                        ci, nt_wrap, dic_bins[transcript_name])


if __name__ == "__main__":
    doctest.testmod()
